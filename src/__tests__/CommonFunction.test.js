import { doPaging } from '../components/FizzBuzz/CommonFunction';

describe('Test do Paging function', () => {
  it('Test do paging', () => {
    let wrapper = doPaging(6, 0, 5);
    expect(wrapper.length).toBe(5);
    wrapper = doPaging(6, 3, 5);
    expect(wrapper.length).toBe(5);
    wrapper = doPaging(6, 5, 5);
    expect(wrapper.length).toBe(5);
  });
});
