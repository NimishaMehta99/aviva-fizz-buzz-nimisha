import React from 'react';
import { shallow } from 'enzyme';
import Pagination from '../components/FizzBuzz/Pagination';

describe('Pagination component tests', () => {
  let wrapper;

  it('Test Pagination component exist', () => {
    wrapper = shallow(<Pagination totalPages={0} currentPage={1} />);
    expect(wrapper.exists()).toBe(true);

    wrapper = shallow(<Pagination totalPages={2} currentPage={1} />);
    expect(wrapper.exists()).toBe(true);
  });
  it('Test Next button click of pagination', () => {
    wrapper.find('#prevBtn').simulate('click');
    wrapper.setProps({ currentPage: 2 });
    wrapper.find('#prevBtn').simulate('click');
  });
  it('Test Previous button click of pagination', () => {
    wrapper.find('#nextBtn').simulate('click');
    wrapper.setProps({ currentPage: 1 });
    wrapper.find('#nextBtn').simulate('click');
    wrapper.setProps({ currentPage: 3 });
    wrapper.find('#nextBtn').simulate('click');
  });
  it('Test page number on click event', () => {
    const mockedEvent = { target: { id: 2 } };
    wrapper
      .find('.page-link')
      .at(2)
      .simulate('click', mockedEvent);
  });
  it('Test First page button of pagination', () => {
    wrapper.find('#firstBtn').simulate('click');
  });
  it('Test Last page button of pagination', () => {
    wrapper.find('#lastBtn').simulate('click');
  });
});
