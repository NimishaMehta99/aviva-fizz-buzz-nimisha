import ValidateForm from '../components/FizzBuzz/Validation';

describe('Validation component', () => {
  let wrapper;
  it('Test Validation component exist', () => {
    wrapper = ValidateForm(10);
    expect(wrapper.formIsValid).toBe(true);
    wrapper = ValidateForm(null);
    expect(wrapper.formIsValid).toBe(false);
    wrapper = ValidateForm(1002);
    expect(wrapper.formIsValid).toBe(false);
  });
});
