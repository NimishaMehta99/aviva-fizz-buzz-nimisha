import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';

describe('App component tests', () => {
  let wrapper;

  it('Test App component exist', () => {
    wrapper = shallow(<App />);
    expect(wrapper.exists()).toBe(true);
  });
});
