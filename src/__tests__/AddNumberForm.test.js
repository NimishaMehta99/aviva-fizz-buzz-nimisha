import React from 'react';
import { shallow } from 'enzyme';
import AddNumberForm from '../components/FizzBuzz/AddNumberForm';

describe('AddNumberForm component tests', () => {
  let wrapper;
  it('Test AddNumberForm component exist', () => {
    wrapper = shallow(<AddNumberForm />);
    expect(wrapper.exists()).toBe(true);
  });

  it('Test input field', () => {
    const inputField = wrapper.find('#inputNumber');
    inputField.simulate('change', {
      target: { id: 'inputNumber', value: 'abc' }
    });
    inputField.simulate('change', {
      target: { id: 'inputNumber', value: '5' }
    });
    expect(wrapper.state().inputNumber).toEqual('5');
  });
  it('Test form onsubmit event', () => {
    const inputField = wrapper.find('#inputNumber');
    inputField.simulate('change', {
      target: { id: 'inputNumber', value: '5' }
    });
    expect(wrapper.state().inputNumber).toEqual('5');
    wrapper.find('form').simulate('submit', {
      preventDefault() {}
    });
  });
  it('Test form onsubmit event for invalid form', () => {
    wrapper
      .find('#inputNumber')
      .simulate('change', { target: { id: 'inputNumber', value: 'abc' } });
    wrapper.find('form').simulate('submit', {
      preventDefault() {}
    });
  });
});
