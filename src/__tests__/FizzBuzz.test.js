import React from 'react';
import { shallow } from 'enzyme';
import FizzBuzz from '../components/FizzBuzz/FizzBuzz';

describe('FizzBuzz component tests', () => {
  let wrapper;
  it('Test FizzBuzz component exist', () => {
    wrapper = shallow(<FizzBuzz />);
    expect(wrapper.exists()).toBe(true);
  });
  it('Check input box exist', () => {
    wrapper.instance().handleSubmit(10);
    expect(wrapper.state().totalPages).toEqual(1);
  });
});
