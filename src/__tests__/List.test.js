import React from 'react';
import { shallow } from 'enzyme';
import List from '../components/FizzBuzz/List';
import Pagination from '../components/FizzBuzz/Pagination';

describe('List component', () => {
  let wrapper;
  const basePropsWednesday = {
    items: [
      { value: 1, id: '1' },
      { value: 2, id: '2' },
      { value: 3, id: '3' },
      { value: 4, id: '4' },
      { value: 5, id: '5' },
      { value: 15, id: '15' }
    ]
  };
  it('Test List component exist for wednesday', () => {
    wrapper = shallow(
      <List
        items={basePropsWednesday.items}
        totalPages={2}
        itemsPerPage={20}
        isWednesday
        currentPage={2}
      />
    );
    expect(wrapper.exists()).toBe(true);
  });
  it('Test List component exist for non-wednesday', () => {
    wrapper.setProps({ currentPage: 1, isWednesday: false });
    expect(wrapper.find(Pagination).length).toEqual(1);
    wrapper
      .find(Pagination)
      .props()
      .setCurrentPage();
  });
});
