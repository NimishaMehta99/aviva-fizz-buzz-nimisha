import React from 'react';
import FizzBuzz from './components/FizzBuzz/FizzBuzz';

const App = () => <FizzBuzz />;

export default App;
