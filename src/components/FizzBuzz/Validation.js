const emptyNumber = 'Please enter number.';
const numberRange = 'Number should be between 1 and 1000';
const ValidateForm = inputNumber => {
  let errorMsg = '';
  let formIsValid = true;
  if (inputNumber === '' || inputNumber === null || inputNumber === undefined) {
    formIsValid = false;
    errorMsg = emptyNumber;
  }
  if (parseInt(inputNumber, 10) < 1 || parseInt(inputNumber, 10) > 1000) {
    formIsValid = false;
    errorMsg = numberRange;
  }
  return { formIsValid, errorMsg };
};

export default ValidateForm;
