import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { checkWednesday } from './CommonFunction';
import Pagination from './Pagination';

const getFormattedText = (value, isWednesday) => {
  const divideBy3 = value % 3 === 0;
  const divideBy5 = value % 5 === 0;
  const fizz = <span className="text-info">{isWednesday ? 'Wizz ' : 'Fizz '} </span>;
  const buzz = <span className="text-success">{isWednesday ? 'Wuzz' : 'Buzz'}</span>;
  const fizzbuzz = (
    <>
      <span className="text-info">{isWednesday ? 'Wizz ' : 'Fizz '} </span>
      <span className="text-success">{isWednesday ? 'Wuzz' : 'Buzz'}</span>
    </>
  );
  switch (true) {
    case divideBy3 && divideBy5: {
      return fizzbuzz;
    }
    case !divideBy3 && divideBy5: {
      return buzz;
    }
    case divideBy3 && !divideBy5: {
      return fizz;
    }
    default:
      return value;
  }
};
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1
    };
  }

  componentWillReceiveProps(newprops) {
    this.setState({ currentPage: newprops.currentPage });
  }

  render() {
    const { currentPage } = this.state;
    const { items, totalPages, itemsPerPage, isWednesday } = this.props;
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = items.slice(indexOfFirstItem, indexOfLastItem);
    return (
      <>
        <ul className="list-group list-group-flush">
          {currentItems.map(item => (
            <li className="list-group-item" key={item.id}>
              {getFormattedText(item.value, isWednesday)}
            </li>
          ))}
        </ul>
        <Pagination
          totalPages={totalPages}
          currentPage={currentPage}
          setCurrentPage={currentpage => {
            this.setState({ currentPage: currentpage });
          }}
        />
      </>
    );
  }
}
List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({ value: PropTypes.number, id: PropTypes.string })),
  totalPages: PropTypes.number,
  isWednesday: PropTypes.bool,
  itemsPerPage: PropTypes.number
};
List.defaultProps = {
  items: [],
  totalPages: 0,
  isWednesday: checkWednesday(),
  itemsPerPage: 20
};
export default List;
