import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ValidateForm from './Validation';

class AddNumberForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputNumber: '',
      error: ''
    };
  }

  handleOnSubmit = e => {
    e.preventDefault();
    const { inputNumber } = this.state;
    const { handleSubmit } = this.props;

    const { formIsValid, errorMsg } = ValidateForm(inputNumber);
    this.setState({ error: errorMsg });
    if (formIsValid) {
      handleSubmit(parseInt(inputNumber, 10));
      this.setState({
        inputNumber: ''
      });
    } else {
      handleSubmit(parseInt(0, 10));
    }
  };

  handleOnChange = e => {
    const re = /^[0-9\b]+$/;
    const inputNum = e.target.value;
    if (inputNum === '' || re.test(inputNum)) {
      this.setState({ inputNumber: inputNum });
    }
  };

  render() {
    const { inputNumber, error } = this.state;

    return (
      <form onSubmit={e => this.handleOnSubmit(e)}>
        <div className="row">
          <div className="col-8">
            <input
              type="text"
              className="form-control"
              id="inputNumber"
              placeholder="Enter number"
              value={inputNumber}
              onChange={event => {
                this.handleOnChange(event);
              }}
            />
          </div>
          <div className="col-4">
            <button type="submit" className="btn btn-success">
              Submit
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="text-danger">{error}</div>
          </div>
        </div>
      </form>
    );
  }
}
AddNumberForm.propTypes = {
  handleSubmit: PropTypes.func
};
AddNumberForm.defaultProps = {
  handleSubmit: () => {}
};
export default AddNumberForm;
