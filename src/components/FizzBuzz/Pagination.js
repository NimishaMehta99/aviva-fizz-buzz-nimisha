import React from 'react';
import PropTypes from 'prop-types';
import { doPaging, handlePagination } from './CommonFunction';

const Pagination = ({ currentPage, totalPages, setCurrentPage }) => (
  <>
    <div className="card-body">
      <ul className="pagination justify-content-center flex-wrap">
        <li className="page-item">
          <button
            className={`page-link${totalPages === 0 ? ' d-none' : ''}`}
            id="firstBtn"
            type="button"
            onClick={event =>
              setCurrentPage(handlePagination(event, 'first', currentPage, totalPages))
            }>
            First
          </button>
        </li>
        <li className={`page-item${currentPage <= 1 ? ' disabled' : ''}`}>
          <button
            className={`page-link${totalPages === 0 ? ' d-none' : ''}`}
            id="prevBtn"
            type="button"
            onClick={event =>
              setCurrentPage(handlePagination(event, 'previous', currentPage, totalPages))
            }>
            Previous
          </button>
        </li>
        {doPaging(totalPages, currentPage, 5).map(object => (
          <li className={`page-item${currentPage === object ? ' active' : ''}`} key={object}>
            <button
              type="button"
              id={object}
              className="page-link"
              onClick={event =>
                setCurrentPage(handlePagination(event, '', currentPage, totalPages))
              }>
              {object}
            </button>
          </li>
        ))}

        <li className={`page-item${currentPage >= totalPages ? ' disabled' : ''}`}>
          <button
            className={`page-link${totalPages === 0 ? ' d-none' : ''}`}
            id="nextBtn"
            type="button"
            onClick={event =>
              setCurrentPage(handlePagination(event, 'next', currentPage, totalPages))
            }>
            Next
          </button>
        </li>
        <li className="page-item">
          <button
            className={`page-link${totalPages === 0 ? ' d-none' : ''}`}
            id="lastBtn"
            type="button"
            onClick={event =>
              setCurrentPage(handlePagination(event, 'last', currentPage, totalPages))
            }>
            Last
          </button>
        </li>
      </ul>
    </div>
  </>
);
Pagination.propTypes = {
  totalPages: PropTypes.number,
  currentPage: PropTypes.number,
  setCurrentPage: PropTypes.func
};
Pagination.defaultProps = {
  totalPages: 0,
  currentPage: 1,
  setCurrentPage: () => {}
};
export default Pagination;
