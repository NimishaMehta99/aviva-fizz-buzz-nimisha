export const checkWednesday = () => {
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const today = new Date().getDay();
  return days[today] === 'Wednesday';
};
export const doPaging = (totalPages, currentPageParam, maxPages) => {
  let currentPage = currentPageParam;
  if (currentPage < 1) {
    currentPage = 1;
  } else if (currentPage > totalPages) {
    currentPage = totalPages;
  }

  let startPage;
  let endPage;
  if (totalPages <= maxPages) {
    startPage = 1;
    endPage = totalPages;
  } else {
    const maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
    const maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
    if (currentPage <= maxPagesBeforeCurrentPage) {
      startPage = 1;
      endPage = maxPages;
    } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
      startPage = totalPages - maxPages + 1;
      endPage = totalPages;
    } else {
      startPage = currentPage - maxPagesBeforeCurrentPage;
      endPage = currentPage + maxPagesAfterCurrentPage;
    }
  }

  const pages = Array.from(Array(endPage + 1 - startPage).keys()).map(i => startPage + i);
  return pages;
};
export const handlePagination = (event, type, currentPage, totalPages) => {
  let updatePage = currentPage;
  switch (type) {
    case 'next': {
      updatePage = currentPage + 1 <= totalPages ? currentPage + 1 : currentPage;
      return updatePage;
    }
    case 'previous': {
      updatePage = currentPage > 1 ? currentPage - 1 : currentPage;
      return updatePage;
    }
    case 'first': {
      updatePage = 1;
      return updatePage;
    }
    case 'last': {
      updatePage = totalPages;
      return updatePage;
    }
    default:
      updatePage = parseInt(event.target.id, 10);
      return updatePage;
  }
};
