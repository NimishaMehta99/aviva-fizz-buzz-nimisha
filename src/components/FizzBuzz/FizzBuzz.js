import React, { Component } from 'react';
import uuidv1 from 'uuid';
import AddNumberForm from './AddNumberForm';
import List from './List';

class FizzBuzz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      totalPages: 0,
      currentPage: 1
    };
  }

  handleSubmit = inputNumber => {
    const itemsPerPage = 20;
    const itemArr = [];
    for (let i = 1; i <= inputNumber; i += 1) {
      itemArr.push({ value: i, id: uuidv1() });
    }
    this.setState({
      items: itemArr,
      totalPages: Math.ceil(itemArr.length / itemsPerPage),
      currentPage: 1
    });
  };

  render() {
    const { items, totalPages, currentPage } = this.state;

    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="card">
              <div className="card-header">FizzBuzz</div>
              <div className="card-body">
                <AddNumberForm handleSubmit={this.handleSubmit} />
              </div>
              <List items={items} totalPages={totalPages} currentPage={currentPage} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FizzBuzz;
